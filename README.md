# vcms5
CMS & useful features for Laravel 5

Installation:

1. Add to project's composer.json:

    
    
```
#!javascript

    "repositories": [
      {
        "type": "vcs",
        "url": "git@bitbucket.org:tsawler/vcms5.git"
      }
    ],
    "require": {
        "php": ">=5.5.9",
        "laravel/framework": "5.1.*",
        "laravelcollective/html": "5.1.*",
        "intervention/image": "2.*",
        "tsawler/laravel-filemanager": "0.5.*",
        "jenssegers/date": "~3.0",
        "tsawler/vcms5": "dev-master"
    },
```

    
    
1. Run `composer update --prefer-dist`

1. Add to project's config/app.php in service provider section:

    
    
```
#!php

    Collective\Html\HtmlServiceProvider::class,
    Jenssegers\Date\DateServiceProvider::class,
    Tsawler\Vcms5\Vcms5ServiceProvider::class,
    Intervention\Image\ImageServiceProvider::class,
```

    
    
3. Add to project's config/app.php in aliases section:


    
```
#!php

    'Form'      => Collective\Html\FormFacade::class,
    'Html'      => Collective\Html\HtmlFacade::class,
    'Image'     => Intervention\Image\Facades\Image::class,
    'Date'      => Jenssegers\Date\Date::class,
```



4. Run `php artisan vendor:publish --force`

5. Run `composer dump-autoload`

6. Run `php artisan migrate`

7. If necessary, touch the logfile to create it

    ```
    touch storage/logs/laravel-YYYY-MM-DD.log
    ```

8. Run `php artisan db:seed`

9. Open `app/User.php' and add the following:

    
    
```
#!php

    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Config;
```


    
10. Also add the following methods to User.php:


    
```
#!php

    /**
    * User Roles
    * @return mixed
    */
    public function roles()
    {
        return $this->hasMany('Tsawler\Vcms5\models\UserRole');
    }
    
    /**
    * User Prefs
    *
    * @return mixed
    */
    public function prefs()
    {
        return $this->hasOne('Tsawler\Vcms5\models\UserPref');
    }
    
    /**
     * Check if User has role
     *
     * @param $role_id
     * @return bool
     */
    public function hasRole($role_id)
    {
        return in_array($role_id, DB::table(Config::get('vcms5.user_roles_table'))
            ->where('user_id', $this->id)
            ->lists('role'));
    }
```


    
11. Add this to the project's app/Http/Kernel.php route middleware:


   
```
#!php

    'auth.admin'     => \Tsawler\Vcms5\Middleware\RedirectIfNotAdminMiddleware::class,
    'auth.pages'     => \Tsawler\Vcms5\Middleware\RedirectIfNotPagesAdminMiddleware::class,
    'auth.blogs'     => \Tsawler\Vcms5\Middleware\RedirectIfNotBlogsAdminMiddleware::class,
    'auth.events'    => \Tsawler\Vcms5\Middleware\RedirectIfNotEventsAdminMiddleware::class,
    'auth.news'      => \Tsawler\Vcms5\Middleware\RedirectIfNotNewsAdminMiddleware::class,
    'auth.faqs'      => \Tsawler\Vcms5\Middleware\RedirectIfNotFaqsAdminMiddleware::class,
    'auth.galleries' => \Tsawler\Vcms5\Middleware\RedirectIfNotGalleriesAdminMiddleware::class,
    'auth.menus'     => \Tsawler\Vcms5\Middleware\RedirectIfNotMenusAdminMiddleware::class,
    'auth.users'     => \Tsawler\Vcms5\Middleware\RedirectIfNotUsersAdminMiddleware::class,
```



12. Add this line to the project's app/Http/Kernel.php middleware array:

    
```
#!php

    \Tsawler\Vcms5\Middleware\SetLanguageMiddleware::class,
```


    
    
13. Make the following directories writable with `chmod -R 777 <dirname>`:

    ```
    public/files
    public/vendor/vcms5/blog
    public/vendor/vcms5/galleries
    public/vendor/vcms5/news
    ```