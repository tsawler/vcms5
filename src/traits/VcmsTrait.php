<?php namespace Tsawler\Vcms5\Traits;

trait VcmsTrait {

    /**
     * Function to test for start of string
     *
     * @return mixed
     */
    private function startsWith($haystack, $needle)
    {
        return $needle === "" || strpos($haystack, $needle) === 0;
    }

}
