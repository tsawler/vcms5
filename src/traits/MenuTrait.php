<?php namespace Tsawler\Vcms5\traits;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\App;
use Tsawler\Vcms5\models\MenuItem;


/**
 * Class MenuTrait
 * @package Tsawler\Vcms5\Traits
 */
trait MenuTrait {

    /**
     * @return mixed
     */
    public static function getMenu()
    {

        if ((Auth::check()) && (Auth::user()->access_level == 3)) {
            $menu = MenuItem::where('menu_id', '=', '1')
                ->orderBy('sort_order')
                ->get();

            return $menu;

        } else {
            if (Cache::has('main_menu_' . App::getLocale())) {
                $menu = Cache::get('main_menu_' . App::getLocale());
            } else {
                $menu = MenuItem::with('dropdownItems', 'targetPage', 'dropdownItems.targetPage')
                    ->where('menu_id', '=', '1')
                    ->where('active', '=', '1')
                    ->orderBy('sort_order')
                    ->get();
                Cache::forever('main_menu_' . App::getLocale(), $menu);
            }

            return $menu;

        }
    }

}
