<?php namespace Tsawler\Vcms5;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Config;

/**
 * Class Vcms5ServiceProvider
 * @package Tsawler\Vcms5
 */
class Vcms5ServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        if (Config::get('vcms5.use_routes') == true)
        {
            include __DIR__ . '/routes.php';
        }

        $this->loadTranslationsFrom(__DIR__.'/lang', 'vcms5');

        $this->loadViewsFrom(__DIR__.'/views', 'vcms5');

        $this->publishes([
            __DIR__ . '/config/vcms5.php' => config_path('vcms5.php', 'config'),
        ], 'v_config');

        $this->publishes([
            __DIR__.'/../public' => public_path('vendor/vcms5'),
        ], 'v_public');

        $this->publishes([
            __DIR__.'/migrations' => base_path('/database/migrations'),
        ], 'v_migrations');

        $this->publishes([
            __DIR__.'/views' => base_path('resources/views/vendor/vcms5'),
        ], 'v_views');

        $this->publishes([
            __DIR__.'/seeds' => base_path('database/seeds'),
        ], 'seeds');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        /* $this->app['vcms5'] = $this->app->share(function ()
        {
            return true;
        });*/
    }

}
