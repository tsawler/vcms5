<?php namespace Tsawler\Vcms5\controllers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\App;
use Illuminate\Routing\Controller;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cache;
use Tsawler\Vcms5\models\News;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Lang;

/**
 * Class VcmsNewsController
 * @package verilion\vcms
 */
class VcmsNewsController extends VcmsBaseController {

    /**
     * Save edited nes item (called via ajax)
     *
     * @return string
     */
    public function saveNews()
    {
        if (Auth::user()->hasRole('news')) {

            $validator = null;

            if (Session::get('lang') == "en") {
                $update_rules = array(
                    'thedata'      => 'required|min:2',
                    'thetitledata' => 'required|min:2|unique:news,title,' . Input::get('news_id')
                );
            } else {
                $update_rules = array(
                    'thedata'      => 'required|min:2',
                    'thetitledata' => 'required|min:2|unique:news,title_fr,' . Input::get('news_id')
                );
            }
            $validator = Validator::make(Input::all(), $update_rules);

            if ($validator->passes()) {

                $news = News::find(Input::get('news_id'));
                if (Session::get('lang') == "fr") {
                    $news->news_text_fr = trim(Input::get('thedata'));
                    $news->title_fr = trim(Input::get('thetitledata'));
                } else {
                    $news->news_text = trim(Input::get('thedata'));
                    $news->title = trim(Input::get('thetitledata'));
                }
                $news->slug = Str::slug(Input::get('thetitledata'));
                $news->save();

                return "Item updated successfully";
            } else {
                return "Error!";
            }
        }
    }


    /**
     * Show a news item
     *
     * @return mixed
     */
    public function showNews()
    {
        $slug = Request::segment(2);
        $news_title = "Not active";
        $news_text = "Either this news item is not active, or it does not exist";
        $active = 1;
        $news_id = 0;

        $results = DB::table('news')->where('slug', '=', $slug)
            ->get();

        foreach ($results as $result) {
            $active = $result->active;
            if (($active > 0) || ((Auth::check()) && (Auth::user()->hasRole('news')))) {
                if ((Session::get('lang') == null) || (Session::get('lang') == "en")) {
                    $news_title = $result->title;
                    $news_text = $result->news_text;
                    $news_id = $result->id;
                } else {
                    $news_title = $result->title_fr;
                    $news_text = $result->news_text_fr;
                    $news_id = $result->id;
                }
                $news_image = $result->image;
                $news_date = $result->news_date;
            }
        }

        return View::make('vcms5::public.news-page')
            ->with('news_title', $news_title)
            ->with('news_text', $news_text)
            ->with('active', $active)
            ->with('news_id', $news_id)
            ->with('news_date', $news_date)
            ->with('news_image', $news_image)
            ->with('menu', $this->menu);
    }


    /**
     * List all news items
     *
     * @return mixed
     */
    public function getAllNews()
    {
        $news = News::where('active', '=', '1')->orderby('title')->get();

        return View::make('vcms5::admin.news-list-all')
            ->with('allnews', $news)
            ->with('page_name', '');
    }


    /**
     * @return mixed
     */
    public function index()
    {
        $news = News::where('active', '=', '1')
            ->where('news_date', '<=', date('Y-m-d'))
            ->orderby('news_date', 'desc')
            ->orderby('title')
            ->paginate(10);

        return View::make('vcms5::public.news-list')
            ->with('news', $news)
            ->with('menu', $this->menu)
            ->with('page_title', Lang::get('vcms5::vcms5.news'));
    }


    /**
     * Show newsitem for edit or add
     *
     * @return mixed
     */
    public function getEditnews()
    {
        $news_id = Input::get('id');

        if ($news_id > 0) {
            $news = News::find($news_id);
        } else {
            $news = new News;
        }

        return View::make('vcms5::admin.news-edit-newsitem')
            ->with('news_id', $news_id)
            ->with('news', $news);
    }


    /**
     * Save edited news item
     *
     * @return mixed
     */
    public function postEditNews()
    {
        $news_id = Input::get('news_id');
        if ($news_id > 0) {
            $news = News::find($news_id);
        } else {
            $news = new News;
        }

        $news->user_id = Auth::user()->id;
        $news->title = Input::get('title');
        $news->news_text = Input::get('news_text');

        if (Config::get('vcms5.use_fr')) {
            $news->title_fr = Input::get('title_fr');
            $news->news_text_fr = Input::get('news_text_fr');
        }
        $news->slug = Str::slug(Input::get('title'));
        $news->active = Input::get('active');

        if (Input::has('news_date')) {
            $news->news_date = Input::get('news_date');
        } else {
            $news->news_date = date('Y-m-d');
        }

        $news->save();
        $news_id = $news->id;

        if (Input::hasFile('image_name')) {
            $file = Input::file('image_name');
            $destinationPath = base_path() . '/public/vendor/vcms5/news/';
            $filename = $file->getClientOriginalName();
            $upload_success = Input::file('image_name')->move($destinationPath, $filename);
            if (!\File::exists($destinationPath . "thumbs")) {
                \File::makeDirectory($destinationPath . "thumbs");
            }

            $thumb_img = Image::make($destinationPath . $filename);
            $height = $thumb_img->height();
            $width = $thumb_img->width();


            if (($height < Config::get('vcms5.min_img_height')) || ($width < Config::get('vcms5.min_img_width'))) {
                \File::delete($destinationPath . $filename);

                return Redirect::to('/admin/news/newsitem?id=' . Input::get('blog_id'))
                    ->with('error', 'Your image is too small. It must be at least '
                        . Config::get('vcms5.min_img_width')
                        . ' pixels wide, and '
                        . Config::get('vcms5.min_img_height')
                        . ' pixels tall!');
            }

            $thumb_img->fit(Config::get('vcms5.thumb_size'), Config::get('vcms5.thumb_size'))
                ->save($destinationPath . "thumbs/" . $filename);

            unset($thumb_img);
            $img = Image::make($destinationPath . $filename);

            $width = $img->width();

            if ($width > Config::get('vcms5.max_blog_img_width')) {
                // this image is very large; we'll need to resize it.
                $img = $img->fit(Config::get('vcms5.max_blog_img_width'),
                    (Config::get('vcms5.max_blog_img_width') * 0.375));
                $img->save();
            }

            if ($upload_success) {
                $news = News::find($news_id);
                $news->image = $filename;
                $news->save();
            }
        }


        if ((Input::has('src')) && (strlen(Input::get('src')) > 0)) {
            return Redirect::to('/admin/news/all-newsitems')
                ->with('message', 'Changes saved');
        } else {
            return Redirect::to('/admin/news/newsitem?id=' . $news_id)
                ->with('message', 'Changes saved');
        }
    }


    /**
     * Delete a news item
     *
     * @return mixed
     */
    public function getDeleteNews()
    {
        $item = News::find(Input::get('id'));
        $item->delete();

        return Redirect::to('/admin/news/all-news')
            ->with('message', 'Item deleted');
    }

}
