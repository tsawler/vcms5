<?php namespace Tsawler\Vcms5\controllers;

use App\User;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image;
use Tsawler\Vcms5\models\UserPref;
use Illuminate\Support\Facades\Hash;

/**
 * Class VcmsProfileController
 * @package Tsawler\Vcms5\controllers
 */
class VcmsProfileController extends Controller {


    /**
     * Display dashboard
     *
     * @return mixed
     */
    public function getProfile()
    {
        return View::make('vcms5::admin.profile')
            ->with('user', Auth::user())
            ->with('prefs', Auth::user()->prefs);
    }


    /**
     * Save profile
     *
     * @return mixed
     */
    public function postProfile()
    {
        $user = User::find(Auth::user()->id);

        $user->first_name = trim(Input::get('first_name'));
        $user->last_name = trim(Input::get('last_name'));
        $user->email = trim(Input::get('email'));
        $user->user_active = Input::get('user_active');

        // only change password if it's changed on the form
        if (strlen(Input::get('password')) > 0) {
            $user->password = Hash::make(Input::get('password'));
        }

        $user->user_active = 1;
        $user->access_level = 3;
        $user->save();

        return Redirect::to('/admin/users/profile')
            ->with('message', 'Changes saved');

    }


    /**
     * Save user prefs
     *
     * @return mixed
     */
    public function postPrefs()
    {

        if (Input::hasFile('image_name')) {
            $file = Input::file('image_name');

            $destinationPath = base_path() . '/public/vendor/vcms5/assets/avatars/';

            if (!\File::exists($destinationPath)) {
                \File::makeDirectory($destinationPath);
            }

            $filename = $file->getClientOriginalName();
            $save_name = "avatar_" . Auth::user()->id . ".jpg";

            $upload_success = Input::file('image_name')->move($destinationPath, $filename);

            if ($upload_success) {
                $img = Image::make($destinationPath . $filename);
                $height = $img->height();
                $width = $img->width();


                if (($height < 48) || ($width < 48)) {
                    return Redirect::to('/admin/users/profile')
                        ->with('error', 'Your image is too small. It must be at least 48 x 48 pixels!');
                    \File::delete($destinationPath . $filename);
                    exit;
                }

                $img->fit(48, 48)
                    ->save($destinationPath . $save_name);

                $userPrefs = UserPref::find(Auth::user()->id);

                if ($userPrefs == null) {
                    $userPrefs = new UserPref;
                    $userPrefs->user_id = Auth::user()->id;
                }

                $userPrefs->avatar = $save_name;
                $userPrefs->save();

            }
        }

        // other fields
        $userPrefs = UserPref::find(Auth::user()->id);

        if ($userPrefs == null) {
            $userPrefs = new UserPref;
            $userPrefs->user_id = Auth::user()->id;
        }

        $userPrefs->username = Input::get('username');
        $userPrefs->save();

        return Redirect::to('/admin/users/profile')
            ->with('message', 'Changes saved');

    }

}
