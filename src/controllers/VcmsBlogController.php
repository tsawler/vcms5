<?php namespace Tsawler\Vcms5\controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;
use Tsawler\Vcms5\Localize;
use Tsawler\Vcms5\models\Blog;
use Tsawler\Vcms5\models\BlogPost;

/**
 * Class VcmsBlogController
 * @package Tsawler\Vcms5\controllers
 */
class VcmsBlogController extends VcmsBaseController {

    /**
     * Get a list of all blogs for admin
     *
     * @return mixed
     */
    public function getAllBlogs()
    {
        $blogs = Blog::orderBy('title')->get();

        return View::make('vcms5::admin.blogs-list-all')
            ->with('allblogs', $blogs)
            ->with('page_name', '')
            ->with('menu', $this->menu);
    }


    /**
     * Display individual post
     *
     * @return mixed
     */
    public function getPost()
    {
        $post = BlogPost::where('slug', '=', Request::segment(3))->firstOrFail();
        //$archives = BlogPost::archives();

        $post_title_field = Localize::localize('title');
        $post_content_field = Localize::localize('content');
        $blog_title_field = Localize::localize('title');
        $blog = $post->blog;

        //dd($blog);

        return View::make('vcms5::public.post')
            ->with('post_title_field', $post_title_field)
            ->with('post_content_field', $post_content_field)
            ->with('page_title', $post->$post_title_field)
            ->with('blog_title_field', $blog_title_field)
            ->with('post', $post)
            ->with('menu', $this->menu);
    }


    /**
     * Get a blog for display
     *
     * @return mixed
     */
    public function getBlog()
    {
        // get blog
        $slug = Request::segment(2);
        $blog = Blog::where('slug', '=', $slug)->firstOrFail();

        // get posts
        $posts = BlogPost::where('blog_id', '=', $blog->id)
            ->where('active', '=', '1')
            ->paginate(10);

        //$archives = BlogPost::archives();

        $blog_title_field = Localize::localize('title');
        $post_title_field = Localize::localize('title');
        $post_summary_field = Localize::localize('summary');

        return View::make('vcms5::public.blog')
            ->with('blog', $blog)
            ->with('posts', $posts)
            ->with('page_title_field', $blog->$blog_title_field)
            ->with('post_title_field', $post_title_field)
            ->with('page_title', $blog->$blog_title_field)
            ->with('post_content_field', $post_summary_field)
            ->with('menu', $this->menu);
    }


    /**
     * Delete a blog
     *
     * @return mixed
     */
    public function getDeleteBlog()
    {
        $item = Blog::find(Input::get('id'));
        $item->delete();

        return Redirect::to('/admin/blogs/all-blogs')
            ->with('message', 'Blog deleted');
    }


    /**
     * Display blog for edit, along with all entries
     */
    public function getEditBlog()
    {
        if (Input::get('id') > 0) {
            $blog = Blog::find(Input::get('id'));
            $items = BlogPost::where('blog_id', '=', Input::get('id'))->get();
        } else {
            $blog = new Blog;
            $items = null;
        }

        return View::make('vcms5::admin.blog-edit-blog')
            ->with('blog', $blog)
            ->with('items', $items)
            ->with('blog_id', Input::get('id'));
    }


    /**
     * Display post for edit
     *
     * @return mixed
     */
    public function getEditPost()
    {
        if (Input::get('id') > 0) {
            $post = BlogPost::find(Input::get('id'));
        } else {
            $post = new BlogPost;
        }

        if (Input::has('src')) {
            $src = Input::get('src');
        } else {
            $src = '';
        }

        $results = Blog::orderBy('title')->get();
        $blogs = array();

        foreach ($results as $result) {
            $blogs[$result->id] = $result->title;
        }

        $results = \App\User::orderBy('last_name')->get();
        $authors = [];

        foreach($results as $item) {
            $authors[$item->id] = $item->first_name . " " . $item->last_name;
        }

        return View::make('vcms5::admin.blog-edit-post')
            ->with('post_id', Input::get('id'))
            ->with('post', $post)
            ->with('blog_id', Input::get('bid'))
            ->with('src', $src)
            ->with('blogs', $blogs)
            ->with('authors', $authors);
    }


    /**
     * Save edited post
     *
     * @return mixed
     */
    public function postEditPost()
    {
        if (Input::get('post_id') > 0) {
            $post = BlogPost::find(Input::get('post_id'));
        } else {
            $post = new BlogPost;
        }

        $post->user_id = Auth::user()->id;
        $post->blog_id = Input::get('blog_id');
        $post->title = Input::get('title');
        $post->summary = Input::get('summary');
        $post->content = Input::get('content');
        if (Config::get('vcms5.use_fr')) {
            $post->title_fr = Input::get('title_fr');
            $post->summary_fr = Input::get('summary_fr');
            $post->content_fr = Input::get('content_fr');
        }
        if (Config::get('vcms5.use_es')) {
            $post->title_es = Input::get('title_es');
            $post->summary_es = Input::get('summary_es');
            $post->content_es = Input::get('content_es');
        }
        $post->slug = Str::slug(Input::get('title'));
        $post->meta = "";
        $post->active = Input::get('active');
        $post->user_id = Input::get('user_id');

        if (Input::has('post_date')) {
            $post->post_date = Input::get('post_date');
        } else {
            $post->post_date = date('Y-m-d');
        }

        $post->save();
        $post_id = $post->id;

        // probably want to extract this to a reusable trait! TODO
        if (Input::hasFile('image_name')) {
            $file = Input::file('image_name');
            $destinationPath = base_path() . '/public/vendor/vcms5/blog/' . Input::get('blog_id') . "/";
            $filename = $file->getClientOriginalName();
            $upload_success = Input::file('image_name')->move($destinationPath, $filename);
            if (!\File::exists($destinationPath . "thumbs")) {
                \File::makeDirectory($destinationPath . "thumbs");
            }

            $thumb_img = \Image::make($destinationPath . $filename);
            $height = $thumb_img->height();
            $width = $thumb_img->width();


            if (($height < Config::get('vcms5.min_img_height')) || ($width < Config::get('vcms5.min_img_width'))) {
                \File::delete($destinationPath . $filename);

                return Redirect::to('/admin/blogs/blog?id=' . Input::get('blog_id'))
                    ->with('error', 'Your image is too small. It must be at least '
                        . Config::get('vcms5.min_img_width')
                        . ' pixels wide, and '
                        . Config::get('vcms5.min_img_height')
                        . ' pixels tall!');
            }

            $thumb_img->fit(Config::get('vcms5.thumb_size'), Config::get('vcms5.thumb_size'))
                ->save($destinationPath . "thumbs/" . $filename);

            unset($thumb_img);
            $img = \Image::make($destinationPath . $filename);

            $width = $img->width();

            if ($width > Config::get('vcms5.max_blog_img_width')) {
                // this image is very large; we'll need to resize it.
                $img = $img->fit(Config::get('vcms5.max_blog_img_width'),
                    Config::get('vcms5.max_blog_img_height'));
                $img->save();
            }

            if ($upload_success) {
                $post = BlogPost::find($post_id);
                $post->image = $filename;
                $post->save();
            }
        }


        if ((Input::has('src')) && (strlen(Input::get('src')) > 0)) {
            return Redirect::to('/admin/blogs/posts')
                ->with('message', 'Changes saved');
        } else {
            return Redirect::to('/admin/blogs/blog?id=' . Input::get('blog_id'))
                ->with('message', 'Changes saved');
        }
    }


    /**
     * Save edited blog
     *
     * @return mixed
     */
    public function postEditBlog()
    {
        $blog_id = Input::get('blog_id');

        if ($blog_id > 0) {
            $blog = Blog::find($blog_id);
        } else {
            $blog = new Blog;
        }

        $blog->title = Input::get('title');
        if (Config::get('vcms5.use_fr')) {
            $blog->title_fr = Input::get('title_fr');
        }
        if (Config::get('vcms5.use_es')) {
            $blog->title_es = Input::get('title_es');
        }
        $blog->active = Input::get('active');
        $blog->slug = Str::slug(Input::get('title'));
        $blog->save();

        return Redirect::to('/admin/blogs/blog?id=' . $blog->id)
            ->with('message', 'Changes saved');
    }


    /**
     * Save edited post
     *
     * @return mixed
     */
    public function postSave()
    {
        $validator = Validator::make(Input::all(), Post::$rules);
        if ($validator->passes()) {
            $post = new Post;
            $post->title = trim(Input::get('title'));
            $post->status = Input::get('status');
            $post->published_date = Input::get('post_date') . " 00:00:01";
            $post->content = Input::get('content');
            $post->summary = Input::get('summary');
            $post->meta_description = Input::get('meta_description');
            $post->meta_keywords = Input::get('meta_keywords');
            $post->in_rss = 1;
            $post->slug = urlencode(trim(Input::get('title')));
            $post->save();

            return Redirect::to('/blog')
                ->with('message', 'Post saved successfully');
        } else {
            return Redirect::to('post/create')
                ->with('message', 'The following errors occurred')
                ->withErrors($validator)
                ->withInput();
        }
    }


    /**
     * Delete blog post
     */
    public function postDelete()
    {
        $post = BlogPost::find(Input::get('post_id'));
        $post->delete();

        return Redirect::to('/blog')
            ->with('message', 'Post deleted successfully');
    }

}
