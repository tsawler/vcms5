<?php namespace Tsawler\Vcms5\controllers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Tsawler\Vcms5\models\Faq;
use Illuminate\Routing\Controller;


/**
 * Class FaqController
 * @package Tsawler\Vcms5\controllers
 */
class VcmsFaqController extends Controller {

    public function __construct()
    {
        //$this->beforeFilter('csrf', array('on' => 'post'));
    }


    /**
     * Show all faqs
     *
     * @return mixed
     */
    public function showFaqs()
    {
        if (Session::has('lang') && (Session::get('lang') == 'fr')) {
            $faqs = Faq::where('active', '=', '1')
                ->orderBy('question_fr', 'asc')
                ->get();
        } else {
            $faqs = Faq::where('active', '=', '1')
                ->orderBy('question', 'asc')
                ->get();
        }

        return View::make('vcms.faqs')
            ->with('faqs', $faqs)
            ->with('page_title', "FAQs");
    }


    /**
     * List all faqs in admin
     *
     * @return mixed
     */
    public function getAllFaqs()
    {
        $faqs = Faq::orderBy('question', 'asc')
            ->get();

        return View::make('vcms5::admin.faqs-all-faqs')
            ->with('faqs', $faqs);
    }


    /**
     * Display Edit or add FAQ
     *
     * @return mixed
     */
    public function editFaq()
    {
        $id = Input::get('id');

        if ($id > 0) {
            $faq = Faq::find($id);
        } else {
            $faq = new Faq;
        }

        return View::make('vcms5::admin.faqs-edit-faq')
            ->with('faq', $faq)
            ->with('faq_id', $id);
    }


    /**
     * Save edited faq
     *
     * @return mixed
     */
    public function postEditFaq()
    {
        $id = Input::get('faq_id');

        if ($id > 0) {
            $faq = Faq::find($id);
        } else {
            $faq = new Faq;
        }

        $faq->question = Input::get('question');
        $faq->answer = Input::get('answer');

        if (Config::get('vcms5.use_fr')) {
            $faq->question_fr = Input::get('question_fr');
            $faq->answer_fr = Input::get('answer_fr');
        }
        $faq->active = Input::get('active');

        $faq->save();

        return Redirect::to('/admin/faqs/all-faqs')
            ->with('message', 'Changes saved');
    }


    /**
     * Delete a FAQ
     *
     * @return mixed
     */
    public function deleteFaq()
    {
        $id = Input::get('id');
        $faq = Faq::find($id);
        $faq->delete();

        return Redirect::to('/admin/faqs/all-faqs')
            ->with('message', 'FAQ deleted');

    }

}
