<?php namespace Tsawler\Vcms5\controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;

/**
 * Class VcmsAdminController
 * @package Tsawler\Vcms5\controllers
 */
class VcmsAdminController extends Controller {

    /**
     * Display dashboard
     *
     * @return mixed
     */
    public function getDashboard()
    {
        return View::make('vcms5::admin.index');
    }


    /**
     * Display unauthorized access page
     *
     * @return mixed
     */
    public function get403()
    {
        return Response::view('vcms5::admin.403', array(), 403);
    }

}
