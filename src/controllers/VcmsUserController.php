<?php namespace Tsawler\Vcms5\controllers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Routing\Controller;
use App\User;
use Tsawler\Vcms5\models\UserRole;
use Tsawler\Vcms5\models\Role;

/**
 * Class VcmsUserController
 * @package Tsawler\Vcms5\controllers
 */
class VcmsUserController extends Controller {


    /**
     * List all users
     *
     * @return mixed
     */
    public function getAllUsers()
    {
        $users = User::orderby('last_name')->paginate(16);

        return View::make('vcms5::admin.users-list-all')
            ->with('allusers', $users)
            ->with('page_name', '');
    }


    /**
     * Show user for edit or add
     *
     * @return mixed
     */
    public function getEditUser()
    {
        $self = false;
        if (Input::has('id'))
        {
            $user_id = Input::get('id');
            if ($user_id > 0)
            {
                $user = User::find($user_id);
            } else
            {
                $user = new User;
            }
        } else
        {
            $user_id = Auth::user()->id;
            $user = User::find($user_id);
            $self = true;
        }

        return View::make('vcms5::admin.users-edit-user')
            ->with('user_id', $user_id)
            ->with('user', $user)
            ->with('self', $self);
    }


    /**
     * Save edited user
     *
     * @return mixed
     */
    public function postEditUser()
    {
        $user_id = Input::get('id');
        if ($user_id > 0)
        {
            $user = User::find($user_id);
        } else
        {
            $user = new User;
        }

        $user->first_name = trim(Input::get('first_name'));
        $user->last_name = trim(Input::get('last_name'));
        $user->email = trim(Input::get('email'));
        $user->user_active = Input::get('user_active');

        // only change password if changed on form
        if (($user_id == 0) || (strlen(Input::get('password')) > 0))
        {
            $user->password = Hash::make(Input::get('password'));
        }
        $user->access_level = 3;

        $user->save();

        if ($user_id > 0)
        {
            return Redirect::to('/admin/users/all-users')
                ->with('message', 'User updated successfully');
        } else
        {
            return Redirect::to('/admin/users/user?id=' . $user->id)
                ->with('message', 'User updated successfully');
        }
    }


    /**
     * Save user roles
     *
     * @return mixed
     */
    public function postEditUserRoles()
    {
        $user_id = Input::get('id');
        if ($user_id > 0)
        {
            $user = User::find($user_id);
        } else
        {
            $user = new User;
        }

        $roles = array();
        $user->roles()->delete();

        foreach (Input::all() as $name => $value)
        {
            if ($this->startsWith($name, "r_"))
            {
                $roles[] = $value;
            }
        }
        foreach ($roles as $role_id)
        {
            $user_role = new UserRole;
            $user_role->user_id = $user_id;
            $user_role->role_id = $role_id;
            $user_role->role = Role::find($role_id)->role;
            $user_role->save();
        }

        return Redirect::to('/admin/users/all-users')
            ->with('message', 'User updated successfully');
    }


    /**
     * Delete user by id
     */
    public function getDeleteUser()
    {
        $user = User::find(Input::get('id'));
        $user->delete();

        return Redirect::to('admin/users/all-users')
            ->with('message', 'User deleted successfully.');
    }


    /**
     * Function to test for start of string
     *
     * @return mixed
     */
    private function startsWith($haystack, $needle)
    {
        return $needle === "" || strpos($haystack, $needle) === 0;
    }

}
