<?php namespace Tsawler\Vcms5\controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Tsawler\Vcms5\Traits\MenuTrait;
use Tsawler\Vcms5\models\Page;

/**
 * Class VcmsSearchController
 * @package Tsawler\Vcms5\controllers
 */
class VcmsSearchController extends Controller {

    use MenuTrait;

    /**
     * Search the site
     *
     * @return mixed
     */
    public function performSearch()
    {
        if (Input::has('q')) {
            $searchterm = '%' . strtoupper(Input::get('q')) . '%';

            if ((Session::get('lang') == "en") || (Session::get('lang') == null)) {
                $results = Page::whereRaw('page_title like upper(?) or page_content like upper(?)',
                    array($searchterm, $searchterm))
                    ->get();
            } else {
                $results = Page::whereRaw('page_title_fr like upper(?) or page_content_fr like upper(?)',
                    array($searchterm, $searchterm))
                    ->get();
            }

            return View::make('vcms5::public.search')
                ->with('page_title', 'Search')
                ->with('meta_tags', '')
                ->with('meta', '')
                ->with('searchterm', $searchterm)
                ->with('results', $results)
                ->with('menu', MenuTrait::getMenu());

        } else {
            return View::make('vcms5::public.search')
                ->with('results', [])
                ->with('menu', MenuTrait::getMenu());
        }
    }
}
