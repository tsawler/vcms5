<?php namespace Tsawler\Vcms5\controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Tsawler\Vcms5\models\Fragment;
use Illuminate\Routing\Controller;

/**
 * Class FragmentsController
 * @package Tsawler\Vcms5\controllers
 */
class VcmsFragmentsController extends Controller {

    /**
     * Save edits to fragment (in place, called via ajax)
     *
     * @return text
     */
    public function postSavefragment()
    {
        if (Auth::user()->hasRole('pages')) {
            if ((Session::has('lang')) && (Session::get('lang') == 'fr')) {
                $fragment = Fragment::find(Input::get('fid'));
                $fragment->fragment_text_fr = trim(Input::get('thedata'));
                $fragment->fragment_title_fr = trim(Input::get('thetitle'));
                $fragment->save();
            } else {
                $fragment = Fragment::find(Input::get('fid'));
                $fragment->fragment_text = trim(Input::get('thedata'));
                $fragment->fragment_title = trim(Input::get('thetitle'));
                $fragment->save();
            }

            return "Page updated successfully";
        }
    }


    /**
     * Get a fragment by id
     *
     * @param $id
     * @return array
     */
    public function getFragment($id)
    {
        $fragment = Fragment::find($id);

        return [
            'id'       => $fragment->id,
            'title'    => $fragment->fragment_title,
            'text'     => $fragment->fragment_text,
            'title_fr' => $fragment->fragment_title_fr,
            'text_fr'  => $fragment->fragment_text_fr
        ];
    }
}
