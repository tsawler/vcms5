<?php namespace Tsawler\Vcms5\controllers;

use Illuminate\Routing\Controller;
use Tsawler\Vcms5\traits\MenuTrait;

/**
 * Class VcmsBaseController
 * @package Tsawler\Vcms5\controllers
 */
class VcmsBaseController extends Controller {

    use MenuTrait;

    protected $menu;

    public function __construct(){
        $this->menu = MenuTrait::getMenu();
    }

}
