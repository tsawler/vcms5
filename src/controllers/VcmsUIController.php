<?php namespace Tsawler\Vcms5\controllers;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Routing\Controller;

/**
 * Class VcmsUIController
 * @package Tsawler\Vcms5\controllers
 */
class VcmsUIController extends Controller {

    /**
     * Expanding admin left nav
     *
     * @return string
     */
    public function menuUp()
    {
        Session::put('mini-navbar', 'true');

        return 'true';
    }


    /**
     * Collapsing admin left nav
     * @return string
     */
    public function menuDown()
    {
        Session::forget('mini-navbar');

        return 'true';
    }

}
