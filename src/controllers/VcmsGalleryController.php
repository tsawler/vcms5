<?php namespace Tsawler\Vcms5\controllers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Tsawler\Vcms5\Localize;
use Tsawler\Vcms5\models\Gallery;
use Tsawler\Vcms5\models\GalleryItem;

/**
 * Class VcmsGalleryController
 * @package Tsawler\Vcms5\controllers
 */
class VcmsGalleryController extends VcmsBaseController {

    /**
     * @return mixed
     */
    public function getAllItems()
    {
        $galleries = Gallery::orderBy('gallery_name')->get();
        $items = GalleryItem::all();

        $gallery_name_field = Localize::localize('gallery_name');
        $item_name_field = Localize::localize('item_name');
        $item_description_field = Localize::localize('item_description');

        return View::make('vcms5::public.gallery')
            ->with('galleries', $galleries)
            ->with('items', $items)
            ->with('page_title', Lang::get('vcms5::vcms5.image_gallery'))
            ->with('item_name_field', $item_name_field)
            ->with('item_description_field', $item_description_field)
            ->with('gallery_name_field', $gallery_name_field)
            ->with('menu', $this->menu);
    }


    /**
     * List all galleries
     *
     * @return mixed
     */
    public function getAllGalleries()
    {
        $galleries = Gallery::orderby('gallery_name')->paginate(16);

        return View::make('vcms5::admin.galleries-list-all')
            ->with('allgalleries', $galleries)
            ->with('page_name', '');
    }


    /**
     * Display gallery for edit, along with all entries
     */
    public function getEditGallery()
    {
        if (Input::get('id') > 0) {
            $gallery = Gallery::find(Input::get('id'));
            $items = GalleryItem::where('gallery_id', '=', $gallery->id)->orderby('item_name')->get();
        } else {
            $gallery = new Gallery;
            $items = null;
        }


        return View::make('vcms5::admin.gallery-edit-gallery')
            ->with('gallery', $gallery)
            ->with('items', $items)
            ->with('gallery_id', Input::get('id'));
    }


    /**
     * Save edited gallery
     *
     * @return mixed
     */
    public function postEditGallery()
    {
        $gallery_id = Input::get('gallery_id');

        if ($gallery_id > 0) {
            $gallery = Gallery::find($gallery_id);
        } else {
            $gallery = new Gallery;
        }

        $gallery->gallery_name = Input::get('gallery_name');
        $gallery->slug = Str::slug(Input::get('gallery_name'));

        if (Config::get('vcms5.use_fr')) {
            $gallery->gallery_name_fr = Input::get('gallery_name_fr');
        }

        if (Config::get('vcms5.use_es')) {
            $gallery->gallery_name_es = Input::get('gallery_name_es');
        }

        $gallery->active = Input::get('active');
        $gallery->save();

        return Redirect::to('/admin/galleries/gallery?id=' . $gallery->id)
            ->with('message', 'Changes saved');
    }


    /**
     * Save Gallery Item
     *
     * @return mixed
     */
    public function postSaveItem()
    {
        // get the file
        $file = Input::file('image_name');
        $gallery_id = Input::get('gallery_id');
        $id = Input::get('gallery_item_id');

        if ((Input::hasFile('image_name') == false) && ($id == 0)) {
            return Redirect::to('/admin/galleries/gallery?id=' . $gallery_id)
                ->with('error', 'You must include an image file!')
                ->withInput();
        }

        if (Input::hasFile('image_name')) {
            $destinationPath = base_path() . '/public/galleries/' . $gallery_id . "/";

            $filename = $file->getClientOriginalName();
            $upload_success = Input::file('image_name')->move($destinationPath, $filename);

            if (!\File::exists($destinationPath . "thumbs")) {
                \File::makeDirectory($destinationPath . "thumbs");
            }

            $thumb_img = Image::make($destinationPath . $filename);
            $height = $thumb_img->height();
            $width = $thumb_img->width();


            if (($height < Config::get('vcms5.min_img_height')) || ($width < Config::get('vcms5.min_img_width'))) {
                return Redirect::to('/admin/galleries/gallery?id=' . $gallery_id)
                    ->with('error', 'Your image is too small. It must be at least '
                        . Config::get('vcms5.min_img_width')
                        . ' pixels wide, and '
                        . Config::get('vcms5.min_img_height')
                        . ' pixels tall!');
                \File::delete($destinationPath . $filename);
                exit;
            }

            $thumb_img->fit(Config::get('vcms5.thumb_size'), Config::get('vcms5.thumb_size'))
                ->save($destinationPath . "thumbs/" . $filename);

            unset($thumb_img);
            $img = Image::make($destinationPath . $filename);

            $width = $img->width();
            if (($width > Config::get('vcms5.max_img_width')) || ($height > Config::get('vcms5.max_image_height'))) {
                // this image is very large; we'll need to resize it.
                $img = $img->fit(Config::get('vcms5.max_img_width'), Config::get('vcms5.max_img_height'));
                $img->save();
            }

            if ($upload_success) {
                if ($id > 0) {
                    $item = GalleryItem::find($id);
                } else {
                    $item = new GalleryItem;
                }
                $item->item_name = Input::get('item_name');
                $item->item_description = Input::get('item_description');
                if (Input::has('item_name_fr')) {
                    $item->item_name_fr = Input::get('item_name_fr');
                    $item->item_description_fr = Input::get('item_description_fr');
                }
                if (Input::has('item_name_es')) {
                    $item->item_name_es = Input::get('item_name_es');
                    $item->item_description_es = Input::get('item_description_es');
                }
                $item->active = Input::get('active');
                $item->image_name = $filename;
                $item->gallery_id = $gallery_id;
                $item->save();

                return Redirect::to('/admin/galleries/gallery?id=' . $gallery_id);

            } else {
                return Redirect::to('/admin/galleries/gallery?id=' . $gallery_id)
                    ->with('error', 'There was an error with your submission!');
            }
        } else {
            $item = GalleryItem::find($id);
            $item->item_name = Input::get('item_name');
            $item->item_description = Input::get('item_description');
            if (Input::has('item_name_fr')) {
                $item->item_name_fr = Input::get('item_name_fr');
                $item->item_description_fr = Input::get('item_description_fr');
            }
            $item->active = Input::get('active');
            $item->gallery_id = $gallery_id;
            $item->save();

            return Redirect::to('/admin/galleries/gallery?id=' . $gallery_id)
                ->with('message', 'Changes saved');
        }
    }


    /**
     * Delete a gallery
     *
     * @return mixed
     */
    public function getDeleteGallery()
    {
        $item = Gallery::find(Input::get('id'));
        $item->delete();

        return Redirect::to('/admin/galleries/all-galleries')
            ->with('message', 'Gallery deleted');
    }


    /**
     * Delete gallery item
     *
     * @return mixed
     */
    public function getDeleteItem()
    {
        $item = GalleryItem::find(Input::get('id'));
        $gallery_id = $item->gallery_id;
        $image_name = $item->image_name;
        \File::delete(base_path() . '/public/galleries/' . $gallery_id . "/" . $image_name);
        \File::delete(base_path() . '/public/galleries/' . $gallery_id . "/thumbs/" . $image_name);
        $item->delete();

        return Redirect::to('/admin/galleries/gallery?id=' . Input::get('gallery_id'))
            ->with('message', 'Item deleted');
    }


    /**
     * Retrieve gallery item as JSON
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRetrieveItem()
    {
        $id = Input::get('id');
        $item = GalleryItem::find($id);
        $eArray = array();
        $eArray[] = array(
            'id'                  => $item->id,
            'item_name'           => $item->item_name,
            'item_description'    => $item->item_description,
            'item_name_fr'        => $item->item_name_fr,
            'item_description_fr' => $item->item_description_fr,
            'active_yn'           => $item->active,
            'image_name'          => $item->image_name,
            'gallery_id'          => $item->gallery_id,
        );

        return Response::json($eArray);
    }

}
