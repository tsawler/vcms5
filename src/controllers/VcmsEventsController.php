<?php namespace Tsawler\Vcms5\controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;
use Tsawler\Vcms5\models\Event;

/**
 * Class EventsController
 * @package Tsawler\Vcms5\controllers
 */
class VcmsEventsController extends VcmsBaseController {


    /**
     * Show calendar in admin tool
     *
     * @return mixed
     */
    public function showCal()
    {
        return View::make('vcms5::public.calendar')
            ->with('page_title', Lang::get('vcms5::vcms5.calendar'))
            ->with('menu', $this->menu);
    }


    /**
     * Show calendar in admin tool
     *
     * @return mixed
     */
    public function showCalForAdmin()
    {
        return View::make('vcms5::admin.calendar');
    }


    /**
     * Move an event
     *
     * @return string
     */
    public function getMoveDate()
    {
        if (Auth::user()->hasRole('events')) {
            $id = Input::get('id');
            $delta = Input::get('delta');
            $deltaminutes = Input::get('deltaminutes');

            $event = Event::findOrFail($id);

            $original_start = $event->start_date;
            $original_end = $event->end_date;

            if ($event->all_day == 1) {
                $start = date("Y-m-d", strtotime($delta . " days", strtotime($original_start)));
                $end = date("Y-m-d", strtotime($delta . " days", strtotime($original_end)));

                $event->start_date = $start;
                $event->end_date = $end;
            } else {
                $start = date("Y-m-d", strtotime($delta . " days", strtotime($original_start)));
                $event->start_date = $start;
                $end = date("Y-m-d", strtotime($delta . " days", strtotime($original_end)));
                $event->end_date = $end;

                $original_start_time = $event->start_time;
                $original_end_time = $event->end_time;
                $original_start_date_time = date("Y-m-d H:i", strtotime($original_start . " " . $original_start_time));
                $original_end_date_time = date("Y-m-d H:i", strtotime($original_end . " " . $original_end_time));

                $start_time = date("H:i", strtotime($deltaminutes . " minutes", strtotime($original_start_date_time)));
                $end_time = date("H:i", strtotime($deltaminutes . " minutes", strtotime($original_end_date_time)));
                $event->start_time = $start_time;
                $event->end_time = $end_time;

            }
            $event->save();
            Cache::flush();

            return "true";
        } else {
            return Redirect::to('/admin/unauthorized', 301);
        }
    }


    /**
     * Get all events as json feed
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getJsonEvents()
    {
        $end = Input::get('end');
        $start = Input::get('start');

        $endDate = date('Y-m-d', strtotime($end));
        $startDate = date('Y-m-d', strtotime($start));

        $rs = DB::table(Config::get('vcms5.events_table'))
            ->select('*')
            ->where('active', '=', 1)
            ->whereRaw("((start_date >= '" . $startDate . "' "
                . " and start_date <= '" . $endDate . "') or "
                . "(end_date >= '" . $startDate . "' "
                . "and end_date <= '" . $endDate . "'))")
            ->get();

        $eArray = array();

        foreach ($rs as $row) {
            if ((Auth::user()) && (Auth::user()->hasRole('events')))
                $editable = true;
            else
                $editable = false;

            if ($row->all_day == 1) {
                if ((Session::has('lang')) && (Session::get('lang') == 'fr')) {
                    $eArray[] = array(
                        'id'         => $row->id,
                        'title'      => $row->event_title_fr,
                        'start'      => $row->start_date,
                        'end'        => $row->end_date,
                        'editable'   => $editable,
                        'data-title' => $row->event_title_fr,
                        'tooltip'    => strip_tags($row->event_text_fr),
                        'url'        => '/event/' . $row->id . '/' . Str::slug('event_title_fr'),
                        'className'  => 'citem',
                    );
                } else {
                    $eArray[] = array(
                        'id'         => $row->id,
                        'title'      => $row->event_title,
                        'start'      => $row->start_date,
                        'end'        => $row->end_date,
                        'editable'   => $editable,
                        'data-title' => $row->event_title,
                        'tooltip'    => strip_tags($row->event_text),
                        'url'        => '/event/' . $row->id . '/' . Str::slug('event_title'),
                        'className'  => 'citem',
                    );
                }
            } else {
                if ((Session::has('lang')) && (Session::get('lang') == 'fr')) {
                    $eArray[] = array(
                        'id'         => $row->id,
                        'title'      => $row->event_title_fr,
                        'start'      => $row->start_date . " " . $row->start_time,
                        'end'        => $row->end_date . " " . $row->end_time,
                        'editable'   => $editable,
                        'allDay'     => false,
                        'data-title' => $row->event_title_fr,
                        'tooltip'    => strip_tags($row->event_text_fr),
                        'url'        => '/event/' . $row->id . '/' . Str::slug('event_title_fr'),
                        'className'  => 'citem',
                        'start_time' => $row->start_time,
                        'end_time'   => $row->end_time,
                    );
                } else {
                    $eArray[] = array(
                        'id'         => $row->id,
                        'title'      => $row->event_title,
                        'start'      => $row->start_date . " " . $row->start_time,
                        'end'        => $row->end_date . " " . $row->end_time,
                        'editable'   => $editable,
                        'allDay'     => false,
                        'data-title' => $row->event_title,
                        'tooltip'    => strip_tags($row->event_text),
                        'url'        => '/event/' . $row->id . '/' . Str::slug('event_title'),
                        'className'  => 'citem',
                        'start_time' => $row->start_time,
                        'end_time'   => $row->end_time,
                    );
                }
            }
        }

        return Response::json($eArray);
    }


    /**
     * Get all events as json feed for admin tool
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getJsonEventsForAdmin()
    {
        $end = Input::get('end');
        $start = Input::get('start');

        $endDate = date('Y-m-d', strtotime($end));
        $startDate = date('Y-m-d', strtotime($start));

        $rs = DB::table(Config::get('vcms5.events_table'))
            ->select('*')
            ->where('active', '=', 1)
            ->whereRaw("((start_date >= '" . $startDate . "' "
                . " and start_date <= '" . $endDate . "') or "
                . "(end_date >= '" . $startDate . "' "
                . "and end_date <= '" . $endDate . "'))")
            ->get();

        $eArray = array();

        foreach ($rs as $row) {
            if ((Auth::user()) && (Auth::user()->hasRole('events')))
                $editable = true;
            else
                $editable = false;

            if ($row->all_day == 1) {
                if ((Session::has('lang')) && (Session::get('lang') == 'fr')) {
                    $eArray[] = array(
                        'id'         => $row->id,
                        'title'      => $row->event_title_fr,
                        'start'      => $row->start_date,
                        'end'        => $row->end_date,
                        'editable'   => $editable,
                        'data-title' => $row->event_title_fr,
                        'tooltip'    => strip_tags($row->event_text_fr),
                        'url'        => 'javascript:editEvent(' . $row->id . ')',
                        'onclick'    => 'editEvent(' . $row->id . ')',
                    );
                } else {
                    $eArray[] = array(
                        'id'         => $row->id,
                        'title'      => $row->event_title,
                        'start'      => $row->start_date,
                        'end'        => $row->end_date,
                        'editable'   => $editable,
                        'data-title' => $row->event_title,
                        'tooltip'    => strip_tags($row->event_text),
                        'url'        => 'javascript:editEvent(' . $row->id . ')',
                        'onclick'    => 'editEvent(' . $row->id . ')',
                    );
                }
            } else {
                if ((Session::has('lang')) && (Session::get('lang') == 'fr')) {
                    $eArray[] = array(
                        'id'         => $row->id,
                        'title'      => $row->event_title_fr,
                        'start'      => $row->start_date . " " . $row->start_time,
                        'end'        => $row->end_date . " " . $row->end_time,
                        'editable'   => $editable,
                        'allDay'     => false,
                        'data-title' => $row->event_title_fr,
                        'tooltip'    => strip_tags($row->event_text_fr),
                        'url'        => 'javascript:editEvent(' . $row->id . ')',
                        'className'  => 'citem',
                        'start_time' => $row->start_time,
                        'end_time'   => $row->end_time,
                        'onclick'    => 'editEvent(' . $row->id . ')',
                    );
                } else {
                    $eArray[] = array(
                        'id'         => $row->id,
                        'title'      => $row->event_title,
                        'start'      => $row->start_date . " " . $row->start_time,
                        'end'        => $row->end_date . " " . $row->end_time,
                        'editable'   => $editable,
                        'allDay'     => false,
                        'data-title' => $row->event_title,
                        'tooltip'    => strip_tags($row->event_text),
                        'url'        => 'javascript:editEvent(' . $row->id . ')',
                        'className'  => 'citem',
                        'start_time' => $row->start_time,
                        'end_time'   => $row->end_time,
                        'onclick'    => 'editEvent(' . $row->id . ')',
                    );
                }
            }
        }

        return Response::json($eArray);
    }


    /**
     * Retrieve event for edit
     */
    public function retrieveEvent()
    {
        if (Auth::user()->hasRole('events')) {
            $event_id = Input::get('id');
            $event = Event::find($event_id);
            $eArray = array();
            $eArray[] = array(
                'id'            => $event->id,
                'title'         => $event->event_title,
                'title_fr'      => $event->event_title_fr,
                'event_text'    => $event->event_text,
                'event_text_fr' => $event->event_text_fr,
                'start_date'    => $event->start_date,
                'end_date'      => $event->end_date,
                'all_day'       => $event->all_day,
                'start_time'    => $event->start_time,
                'end_time'      => $event->end_time,
            );

            return Response::json($eArray);
        } else {
            return Redirect::to('/admin/unauthorized', 301);
        }
    }


    /**
     * Save event -- called via ajax, from public facing calendar
     */
    public function postSaveEvent()
    {
        if (Auth::user()->hasRole('events')) {
            if (Input::get('event_id') == 0) {
                $event = new Event;
                //$end = date("Y-m-d", strtotime("1 days", strtotime(Input::get('end_date'))));
                $end = Input::get('end_date');
                $event->end_date = $end;
            } else {
                $event = Event::find(Input::get('event_id'));
                $end = Input::get('end_date');
                $event->end_date = $end;
            }

            $event->start_date = Input::get('start_date');
            $event->all_day = Input::get('all_day');
            if (Input::get('all_day') == 0) {
                if (strlen(Input::get('start_time')) > 0) {
                    $event->start_time = Input::get('start_time');
                } else {
                    $event->start_time = "00:00:01";
                }

                if (strlen(Input::get('end_time')) > 0) {
                    $event->end_time = Input::get('end_time');
                } else {
                    $event->end_time = "11:59:59";
                }
            } else {
                $event->start_time = null;
                $event->end_time = null;
            }


            if ((Session::has('lang')) && (Session::get('lang') == 'fr')) {
                $event->event_title_fr = Input::get('event_title');
                $event->event_text_fr = Input::get('event_text');
                if (Input::get('id') == 0) {
                    $event->event_title = Input::get('event_title');
                    $event->event_text = Input::get('event_text');
                }
            } else if ((Session::has('lang')) && (Session::get('lang') == 'fr')) {
                $event->event_title = Input::get('event_title');
                $event->event_text = Input::get('event_text');
                if (Input::get('id') == 0) {
                    $event->event_title_fr = Input::get('event_title');
                    $event->event_text_fr = Input::get('event_text');
                }
            } else {
                $event->event_title_fr = Input::get('event_title');
                $event->event_text_fr = Input::get('event_text');
                $event->event_title = Input::get('event_title');
                $event->event_text = Input::get('event_text');
            }
            $event->active = 1;

            $event->save();
            Cache::flush();

            return "saved";
        } else {
            return Redirect::to('/admin/unauthorized', 301);
        }
    }


    /**
     * Show an event
     *
     * @return mixed
     */
    public function showEvent()
    {
        $event = Event::findOrFail(Request::segment(2));

        return View::make('vcms.event')
            ->with('event', $event);
    }


    /**
     * Delete an event
     *
     * @return string
     */
    public function deleteEvent()
    {
        if (Auth::user()->hasRole('events')) {
            $event = Event::find(Input::get('id'));
            $event->delete();
            Cache::flush();

            return "deleted";
        } else {
            return Redirect::to('/admin/unauthorized', 301);
        }
    }


    /**
     * Change duration of event
     *
     * @return string
     */
    public function getMoveEndDate()
    {
        $id = Input::get('id');
        $delta = Input::get('delta');
        $deltaminutes = Input::get('deltaminutes');

        $event = Event::findOrFail($id);;
        $original_end = $event->end_date;

        if ($event->all_day == 1) {
            $end = date("Y-m-d", strtotime($delta . " days", strtotime($original_end)));
            $event->end_date = $end;
        } else {
            $end = date("Y-m-d", strtotime($delta . " days", strtotime($original_end)));
            $event->end_date = $end;

            $original_end_time = $event->end_time;
            $original_end_date_time = date("Y-m-d H:i", strtotime($original_end . " " . $original_end_time));

            $end_time = date("H:i", strtotime($deltaminutes . " minutes", strtotime($original_end_date_time)));
            $event->end_time = $end_time;
        }
        $event->save();
        Cache::flush();

        return "true";
    }

}
