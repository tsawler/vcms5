<?php namespace Tsawler\Vcms5\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

/**
 * Class MenuDropdownItem
 * @package Tsawler\Vcms5\models
 */
class MenuDropdownItem extends Model {

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->table = Config::get('vcms5.menu_dropdown_items_table');
    }

    /**
     * @var array
     */
    public static $rules = array(
        'menu_text' => 'required|min:2'
    );

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * Get info from pages table for a record.
     *
     * @return mixed
     */
    public function targetPage()
    {
        return $this->hasOne('Tsawler\Vcms5\models\Page', 'id', 'page_id');
    }
}
