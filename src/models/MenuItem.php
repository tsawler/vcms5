<?php namespace Tsawler\Vcms5\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

/**
 * Class MenuItem
 * @package Tsawler\Vcms5\models
 */
class MenuItem extends Model {

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->table = Config::get('vcms5.menus_items_table');
    }

    /**
     * @var array
     */
    public static $rules = array(
        'menu_text' => 'required|min:2'
    );


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;


    /**
     * Dropdown items for this menu
     *
     * @return mixed
     */
    public function dropdownItems()
    {
        return $this->hasMany('Tsawler\Vcms5\models\MenuDropdownItem')->orderBy('sort_order');;
    }


    /**
     * Get info from pages table for the record.
     *
     * @return mixed
     */
    public function targetPage()
    {
        return $this->hasOne('Tsawler\Vcms5\models\Page', 'id', 'page_id');
    }
}
