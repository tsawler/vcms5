<?php namespace Tsawler\Vcms5\models;

use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Fragment
 * @package Tsawler\Vcms5\models
 */
class Fragment extends Model {

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->table = Config::get('vcms5.fragments_table');
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;


}