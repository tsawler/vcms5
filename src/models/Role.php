<?php namespace Tsawler\Vcms5\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Illuminate\Auth;

/**
 * Class Role
 * @package verilion\vcms
 */
class Role extends Model {

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->table = Config::get('vcms5.roles_table');
    }


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;


    /**
     * @return mixed
     */
    public function users()
    {
        return $this->belongsToMany('User');
    }

}
