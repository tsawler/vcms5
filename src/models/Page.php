<?php namespace Tsawler\Vcms5\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

/**
 * Class Page
 * @package Tsawler\Vcms5\models
 */
class Page extends Model {

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->table = Config::get('vcms5.pages_table');
    }

    /**
     * @var array
     */
    protected $guarded = array('*');

    /**
     * @var array
     */
    public static $rules = array(
        'page_title' => 'min:2|unique:pages',
        'page_name'  => 'unique:pages'
    );

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function fragments()
    {
        return $this->hasMany('Tsawler\Vcms5\models\Fragment', 'page_id', 'id');
    }

}
