<?php namespace Tsawler\Vcms5\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

/**
 * Class Faq
 * @package Tsawler\Vcms5\models
 */
class Faq extends Model {

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->table = Config::get('vcms5.faqs_table');
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

}
