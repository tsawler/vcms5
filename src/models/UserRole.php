<?php namespace Tsawler\Vcms5\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

/**
 * Class UserRole
 * @package Tsawler\Vcms5\models
 */
class UserRole extends Model {

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->table = Config::get('vcms5.user_roles_table');
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;


    /**
     * Link to user
     *
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }


    /**
     * Link to roles
     *
     * @return mixed
     */
    public function role()
    {
        return $this->belongsTo('tsawler\vcms5\Role', 'role_id', 'id');
    }

}
