<?php namespace Tsawler\Vcms5\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

/**
 * Class UserPref
 * @package Tsawler\Vcms5\models
 */
class UserPref extends Model {

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->table = Config::get('vcms5.user_prefs_table');
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;


    /**
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo('User', 'user_id', 'id');
    }

}
