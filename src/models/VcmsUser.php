<?php namespace Tsawler\Vcms5\models;

use Illuminate\Support\Facades\Config;
use Tsawler\Vcms5\Traits\VcmsTrait;

/**
 * Class VcmsUser
 * @package Tsawler\Vcms5\models
 */
class VcmsUser extends Eloquent implements \UserInterface, \RemindableInterface {

    use \UserTrait, \RemindableTrait;
    use VcmsTrait;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');


    /**
     * User Roles
     * @return mixed
     */
    public function roles()
    {
        return $this->hasMany('Tsawler\Vcms5\models\UserRole');
    }


    /**
     * User Prefs
     *
     * @return mixed
     */
    public function prefs()
    {
        return $this->hasOne('Tsawler\Vcms5\models\UserPref');
    }


    /**
     * Check if User has role
     *
     * @param $role_id
     * @return bool
     */
    public function hasRole($role_id)
    {
        return in_array($role_id, DB::table(Config::get('vcms5.user_roles_table'))
            ->where('user_id', $this->id)
            ->lists('role'));
    }

}
