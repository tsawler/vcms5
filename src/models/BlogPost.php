<?php namespace Tsawler\Vcms5\models;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Session;
use Jenssegers\Date\Date;

/**
 * Class BlogPost
 * @package Tsawler\Vcms5\models
 */
class BlogPost extends Model {

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->table = \Config::get('vcms5.blog_posts_table');
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;


    /**
     * Link to parent blog
     *
     * @return mixed
     */
    public function blog()
    {
        return $this->hasOne('Tsawler\Vcms5\models\Blog', 'id', 'blog_id');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeLive($query)
    {
        return $query->where('active', '=', 1)
            ->where('post_date', '<=', date('Y-m-d'));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    /**
     * @return string
     */
    public function postDate()
    {
        $now = Carbon::now();
        $week_ago = Carbon::now()->subDay(7);

        if (Session::get('lang') == 'fr')
            Lang::setLocale('fr');
        if (Session::get('lang') == 'es')
            Lang::setLocale('es');
        $dt = Date::createFromDate(
            date('Y', strtotime($this->post_date)),
            date('m',strtotime($this->post_date)),
            date('d', strtotime($this->post_date))
        );
        if ($dt->between($now, $week_ago))
        {
            return $dt->diffForHumans();
        } else
        {
            return $dt->format('j F Y');
        }
    }

    /**
     * Get list of archived posts
     *
     * @return array
     */
    public static function archives()
    {
//        $query = "
//            select distinct on (grouper)
//                to_char(post_date, 'YYYY') as year,
//                to_char(post_date, 'MM') as month,
//                to_char(post_date, 'YYYYMM') as grouper,
//                to_char(post_date, 'Month') as month_name,
//                  (select count(id) from
//                    posts
//                    where
//                      cast(EXTRACT(MONTH FROM post_date) as integer)
//                        = cast(to_char(post_date, 'MM') as integer)
//                      and cast(EXTRACT(year from post_date) as integer)
//                        = cast(to_char(post_date, 'YYYY') as integer)) as count
//            from
//                posts
//            where
//                active = 1
//            order by
//                grouper, year, month asc
//        ";


//
//        $archives = $results = DB::select($query);
//
//        $results = array();
//        foreach ($archives as $archive)
//        {
//            $results[$archive->year][$archive->month] = array(
//                'monthname' => $archive->month_name,
//                'count'     => $archive->count,
//            );
//        }
//
//        return $results;


    }

}
