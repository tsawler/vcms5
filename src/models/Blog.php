<?php namespace Tsawler\Vcms5\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

/**
 * Class Blog
 * @package Tsawler\Vcms5\models
 */
class Blog extends Model {

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->table = Config::get('vcms5.blogs_table');
    }


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;


    /**
     * Posts for blog
     *
     * @return mixed
     */
    public function posts()
    {
        return $this->hasMany('Tsawler\Vcms5\models\Posts', 'blog_id', 'id');
    }

}