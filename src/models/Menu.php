<?php namespace Tsawler\Vcms5\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

/**
 * Class Menu
 * @package Tsawler\Vcms5\models
 */
class Menu extends Model {

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = Config::get('vcms5.menus_table');
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

}
