<?php namespace Tsawler\Vcms5;

use Illuminate\Support\Facades\Session;


/**
 * Class Localize
 * @package Tsawler\Vcms5
 */
class Localize {

    /**
     * Give correct field name based on base field name and lang in session
     *
     * @param $field
     * @return string
     */
    public static function localize($field)
    {
        if (Session::get('lang') == 'en')
            return $field;
        else
            return $field . "_" . Session::get('lang');

    }

}
