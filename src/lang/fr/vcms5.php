<?php

return [
    'language_switch' => 'English',
    'lang'            => '/changelanguage?lang=en',

    'home'            => 'Accueil',
    'about_us'        => 'A Propos de Nous',
    'contact_us'      => 'Nous Joindre',
    'calendar'        => 'Calendrier',
    'image_gallery'   => 'Galerie d’images',

    'newsletter'      => 'Bulletin',

    'address'         => 'Addresse',
    'email'           => 'Courriel',
    'phone'           => 'Téléphone',
    'fax'             => 'Fax',

    'delete'          => 'Effacer',
    'cancel'          => 'Annuler',
    'save'            => 'Enregistrer',

    'read_more'       => 'En savoir plus',
];