<?php

return [
    'language_switch' => 'Français',
    'lang'            => '/changelanguage?lang=fr',

    'home'            => 'Incio',
    'about_us'        => 'Acerca de Nosotros',
    'contact_us'      => 'Contáctanos',
    'calendar'        => 'Calendario',
    'image_gallery'   => 'Galería de imágenes',

    'newsletter'      => 'Hoja informativa',

    'address'         => 'Dirección',
    'email'           => 'De Correo',
    'phone'           => 'Teléfono',
    'fax'             => 'Fax',

    'delete'          => 'Borrar',
    'cancel'          => 'Cancelar',
    'save'            => 'Guardar',

    'read_more'       => 'Leer más',
];