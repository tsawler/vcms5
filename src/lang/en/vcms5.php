<?php

return [
    'language_switch' => 'Français',
    'lang'            => '/changelanguage?lang=fr',

    'home'            => 'Home',
    'about_us'        => 'About Us',
    'contact_us'      => 'Contact Us',
    'calendar'        => 'Calendar',
    'image_gallery'   => 'Gallery',
    'news'            => 'News',

    'newsletter'      => 'Newsletter',

    'address'         => 'Address',
    'email'           => 'Email',
    'phone'           => 'Phone',
    'fax'             => 'Fax',


    'delete'          => 'Delete',
    'cancel'          => 'Cancel',
    'save'            => 'Save',

    'read_more'       => 'Read More',
];
